#!/usr/bin/env python3.8


# Endpoint


from digi.xbee.devices import XBeeDevice

import time

PORT = "/dev/ttyUSB0"  # com port

BAUD_RATE = 9600

device = XBeeDevice(PORT, BAUD_RATE)


def main():
    try:

        device.open()

        print("Waiting for data\n")

        device.add_data_received_callback(data_recieve_callback)

        time.sleep(25)  # seriel connection will close after 25 seconds



    finally:

        if device is not None and device.is_open():
            device.send_data_broadcast("End point has closed serial connection!")

            print("Closing serial connection")

            device.close()


def data_recieve_callback(xbee_message):
    print("Message recieved: ", xbee_message.data.decode())


if __name__ == '__main__':
    main()



