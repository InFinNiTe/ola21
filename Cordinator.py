#Cordinator

from digi.xbee.devices import XBeeDevice
from digi.xbee.models.options import DiscoveryOptions
from digi.xbee.models.status import NetworkDiscoveryStatus
import time

class communication():

    def __init__(self, uart_device):
        self.local_uart = uart_device
        self.discover()

    def interface(self):
        pass

    def discover(self):
        print("Xbee sending API FRAME to discover network: ")

        try:
            self.xnet = self.local_uart.get_network()
            self.xnet.set_discovery_options({DiscoveryOptions.DISCOVER_MYSELF})
            self.xnet.set_discovery_timeout(20)
            self.xnet.clear()
            self.xnet.add_device_discovered_callback(self.callback_device_discovered)
            self.xnet.add_discovery_process_finished_callback(self.callback_discovery_finished)
            self.xnet.start_discovery_process()
            print("searching for xbee devices")

            while self.xnet.is_discovery_running():
                time.sleep(0.5)
            #self.local_uart.send_data(remote, 'hej')


        except:
            print("Check if xbee is connected!")

    def callback_device_discovered(self, remote):
            print("This device was found:", remote)

    def callback_discovery_finished(self, status):
        if status == NetworkDiscoveryStatus.SUCCESS:
            print("There was no problems detected during discovery")
        else:
            print("Something went wrong with discovery: %s %status.description")

#for send and recieve
    def data_recieve_callback(self, xbee_message):

        print("Message recieved from endpoint: ", xbee_message.data.decode())



